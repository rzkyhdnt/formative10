select product.stock, manufacturer.name as Manufacture, address.street as Address, brand.name as Brand, price.amound as Price, valuta.code as Valuta
from product
join manufacturer on product.manufactureId = manufacturer.id
join address on product.manufactureId = address.id	
join brand on product.brandId = brand.id
join price on product.id = price.productId
join valuta on price.valutaId = valuta.id
where product.stock = 0